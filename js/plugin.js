/*global $, alert, console  */

//slider
var $item = $('.carousel .item');
var $wHeight = $(window).height();
$item.eq(0).addClass('active');
$item.height($wHeight);
$item.addClass('full-screen');

$('.carousel img').each(function () {
    'use strict';
    var $src = $(this).attr('src');
    var $color = $(this).attr('data-color');
    $(this).parent().css({
        'background-image' : 'url(' + $src + ')',
        'background-color' : $color
    });
    $(this).remove();
});

$(window).on('resize', function () {
    'use strict';
    $wHeight = $(window).height();
    $item.height($wHeight);
});

$('.carousel').carousel({
    interval: 4000,
    pause: "false"
});

// navbar 

$('.navbar-nav .nav-item').on('click', function () {
    'use strict';
    $(this).addClass('active').siblings().removeClass('active');
});


//navbar fixed scrolling

var  mn = $(".navbar");
var  mns = "navbar-fixed-top";
var  hdr = $('#mycarousel').height();

$(window).scroll(function () {
    'use strict';
    if ($(this).scrollTop() > (hdr)) {
        mn.addClass(mns);
    } else {
        mn.removeClass(mns);
    }
});

/*SMOTH SCROLL*/
$(function () {
    'use strict';
    $('navbar-nav ul li').click(function () {
        if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {

            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 500);
                return false;
            }
        }
    });
});



// instalize smooth scroll
var scroll = new SmoothScroll('a[href*="#"]', {
    speed: 2000,
    offset: 20
});



// tabs plugin

$(function () {
    "use strict";
    $(".port ul li").click(function () {
        $(this).addClass('active').siblings().removeClass('active');
        $(".port-box .img").hide();
        $($(this).data('filter')).show();
    });
});


// blogs 

$('.blogs .show').click(function () {
    'use strict';
        
    $('.blog-box').fadeIn(1000).removeClass('hidden');
    $(this).addClass('hidden');
});


// Scroll to Top 

$(window).scroll(function () {
    'use strict';
    if ($(this).scrollTop() >= 400) {
        $('#return-to-top').fadeIn(200);
    } else {
        $('#return-to-top').fadeOut(200);
    }
});
$('#return-to-top').click(function () {
    'use strict';
    $('body,html').animate({
        scrollTop : 0
    }, 800);
});


// loading page
$(window).on('load', function () {
    'use strict';
    $('.loading .sk-cube-grid').fadeOut(800, function () {
        $(this).parent().fadeOut(800, function () {
            $('body').css('overflow', 'auto');
            $(this).remove();
        });
    });
});

